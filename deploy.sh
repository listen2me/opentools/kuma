#!/bin/bash
# *********************************************************
# * Author        : LEI Sen
# * Email         : sen.lei@outlook.com
# * Create time   : 2023-02-24 10:06
# * Last modified : 2023-02-24 10:06
# * Filename      : deploy.sh
# * Description   : 
# *********************************************************

docker-compose -f docker-compose.yml up -d --build
